// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/thriqon/vinci/pkg/config"
)

type ImportSpec struct {
	Path string
}

var _ Spec = ImportSpec{}

func NewImportSpecV1(path string) (Spec, error) {
	return ImportSpec{
		Path: path,
	}, nil
}

func (is ImportSpec) getLayerSpecs(ctx context.Context) ([]Spec, error) {
	logger := L(ctx)
	logger.Trace("getLayerSpecs")

	f, err := os.Open(filepath.Join(BaseDir(ctx), is.Path))
	if err != nil {
		return nil, fmt.Errorf("read imported build file %s: %w", is.Path, err)
	}

	defer f.Close()

	cfg, err := config.Load(ctx, f)
	if err != nil {
		return nil, fmt.Errorf("decode imported build file %s: %w", is.Path, err)
	}

	specs := make([]Spec, len(cfg.LayerSpecs))
	for i := range cfg.LayerSpecs {
		specs[i], err = AsSpec(cfg.LayerSpecs[i])
		if err != nil {
			return nil, fmt.Errorf("spec-ify %v: %w", cfg.LayerSpecs[i], err)
		}
	}

	return specs, nil
}

func (is ImportSpec) EnsureInCache(ctx context.Context) ([]imagev1.Descriptor, error) {
	ctx = logWith(ctx, "imported-from", is.Path)
	logger := L(ctx)

	logger.Trace("import-spec")

	specs, err := is.getLayerSpecs(ctx)
	if err != nil {
		return nil, err
	}

	res := []imagev1.Descriptor{}
	for _, spec := range specs {
		descs, err := spec.EnsureInCache(ctx)
		if err != nil {
			return nil, fmt.Errorf("import to cache: %w", err)
		}

		res = append(res, descs...)
	}

	return res, nil
}
