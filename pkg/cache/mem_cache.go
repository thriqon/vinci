// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cache

import (
	"bytes"
	"context"

	// Digest package uses this, but does not require it.
	_ "crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	digest "github.com/opencontainers/go-digest"
)

type memcacheEntry struct {
	Bytes    []byte
	Metadata map[string][]byte
}

type memCache struct {
	entries map[digest.Digest]memcacheEntry
}

func NewMemCache() Cache {
	cache := memCache{
		entries: make(map[digest.Digest]memcacheEntry),
	}

	return &cache
}

func (mc *memCache) IsCached(ctx context.Context, key digest.Digest) (bool, error) {
	_, ok := mc.entries[key]

	return ok, nil
}

func (mc *memCache) Open(ctx context.Context, key digest.Digest) (io.ReadCloser, int64, error) {
	bs, ok := mc.entries[key]
	if !ok {
		return nil, 0, os.ErrNotExist
	}

	rc := ioutil.NopCloser(bytes.NewReader(bs.Bytes))

	return rc, int64(len(bs.Bytes)), nil
}

func (mc *memCache) ReadMeta(ctx context.Context, dgst digest.Digest, key string) ([]byte, error) {
	e, ok := mc.entries[dgst]
	if !ok {
		return nil, fmt.Errorf("read entry %s: %w", dgst, os.ErrNotExist)
	}

	val, ok := e.Metadata[key]
	if !ok {
		return nil, fmt.Errorf("read key %s from %s: %w", key, dgst, os.ErrNotExist)
	}

	return val, nil
}

type memCacheWriter struct {
	entryMap map[digest.Digest]memcacheEntry

	isClosed  bool
	isSettled bool

	digester digest.Digester
	size     int64
	buf      *bytes.Buffer

	metas map[string][]byte
}

func (mcw *memCacheWriter) Write(bs []byte) (int, error) {
	if mcw.isClosed {
		return 0, fmt.Errorf("unable to write to cache: %w", ErrWrongState)
	}

	_, _ = mcw.digester.Hash().Write(bs)

	// also documented to never return an error
	n, _ := mcw.buf.Write(bs)
	mcw.size += int64(n)

	return n, nil
}

func (mcw *memCacheWriter) Close() error {
	mcw.isClosed = true

	return nil
}

func (mcw *memCacheWriter) Digest() digest.Digest {
	if !mcw.isClosed {
		panic("wrong state, not closed")
	}

	return mcw.digester.Digest()
}

func (mcw *memCacheWriter) Size() int64 {
	if !mcw.isClosed {
		panic("wrong state, not closed")
	}

	return mcw.size
}

func (mcw *memCacheWriter) Commit() error {
	if !mcw.isClosed {
		panic("wrong state, not closed")
	}

	if mcw.isSettled {
		panic("wrong state, already settled")
	}

	mcw.entryMap[mcw.Digest()] = memcacheEntry{mcw.buf.Bytes(), mcw.metas}
	mcw.isSettled = true

	return nil
}

func (mcw *memCacheWriter) Discard() error {
	if !mcw.isClosed {
		panic("wrong state, not closed")
	}

	if mcw.isSettled {
		panic("wrong state, already settled")
	}

	mcw.isSettled = true

	return nil
}

func (mcw *memCacheWriter) SetMeta(ctx context.Context, key string, val []byte) error {
	mcw.metas[key] = val

	return nil
}

var _ EntryWriter = &memCacheWriter{}

func (mc *memCache) Create(ctx context.Context) (EntryWriter, error) {
	return &memCacheWriter{
		entryMap: mc.entries,
		digester: digest.SHA256.Digester(),
		size:     int64(0),
		buf:      new(bytes.Buffer),
		metas:    make(map[string][]byte),
	}, nil
}
