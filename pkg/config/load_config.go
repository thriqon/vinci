// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"context"
	"errors"
	"fmt"
	"io"

	v1config "gitlab.com/thriqon/vinci/pkg/config/v1"
	yaml "gopkg.in/yaml.v2"
)

var errUnsupportedVersion = errors.New("version is not supported by vinci")

func Load(ctx context.Context, r io.Reader) (v1config.SingleImageConfigFile, error) {
	var config v1config.SingleImageConfigFile
	if err := yaml.NewDecoder(r).Decode(&config); err != nil {
		return v1config.SingleImageConfigFile{}, fmt.Errorf("load config: %w", err)
	}

	if config.Version != "v1" {
		return v1config.SingleImageConfigFile{}, fmt.Errorf("config file with version %s: %w", config.Version, errUnsupportedVersion)
	}

	return config, nil
}
