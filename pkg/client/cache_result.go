// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"fmt"
	"io"
	"net/http"

	digest "github.com/opencontainers/go-digest"
)

const ContentType = "content-type"

func (c *Client) getToCache(ctx context.Context, cl *http.Client, u string, options ...requestOption) (*digest.Digest, error) { //nolint:funlen // TODO shorter funcs
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to prepare request: %w", err)
	}

	for _, opt := range options {
		if err := opt(ctx, req, nil, nil); err != nil {
			return nil, fmt.Errorf("unable to apply options: %w", err)
		}
	}

	res, err := cl.Do(req)
	if err != nil {
		return nil, fmt.Errorf("unable to GET target: %w", err)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unable to cache result due to '%s': %w", res.Status, ErrUnexpectedStatusCode)
	}

	ew, err := c.blobCache.Create(ctx)
	if err != nil {
		return nil, fmt.Errorf("unable to open file in cache: %w", err)
	}

	_, err = io.Copy(ew, res.Body)
	if err != nil {
		ew.Close()
		_ = ew.Discard()

		return nil, fmt.Errorf("unable to copy file to cache: %w", err)
	}

	err = ew.Close()
	if err != nil {
		_ = ew.Discard()

		return nil, fmt.Errorf("unable to close file: %w", err)
	}

	d := ew.Digest()
	sentDigest := res.Header.Get("Docker-Content-Digest")
	if sentDigest != "" && sentDigest != d.String() {
		_ = ew.Discard()

		return nil, fmt.Errorf("illegal digest sent from server in header: header=%s requested=%s: %w",
			sentDigest, d, ErrInvalidDigestFromServer)
	}

	for _, opt := range options {
		if err := opt(ctx, nil, &d, nil); err != nil {
			_ = ew.Discard()

			return nil, fmt.Errorf("unable to validate response: %w", err)
		}
	}

	if err := ew.SetMeta(ctx, ContentType, []byte(res.Header.Get("Content-Type"))); err != nil {
		_ = ew.Discard()

		return nil, fmt.Errorf("set '%s' to '%s': %w", ContentType, res.Header.Get("Content-Type"), err)
	}

	for _, opt := range options {
		if err := opt(ctx, nil, nil, ew); err != nil {
			_ = ew.Discard()

			return nil, fmt.Errorf("option for cache writer: %w", err)
		}
	}

	err = ew.Commit()
	if err != nil {
		_ = ew.Discard()

		return nil, fmt.Errorf("unable to commit file: %w", err)
	}

	return &d, nil
}
