// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	digest "github.com/opencontainers/go-digest"
)

func (c *Client) uploadFromCache(ctx context.Context, cl *http.Client, u string, dgst digest.Digest, options ...requestOption) error {
	rc, size, err := c.blobCache.Open(ctx, dgst)
	if err != nil {
		return fmt.Errorf("unable to open blob: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPut, u, rc)
	if err != nil {
		return fmt.Errorf("unable to prepare upload %s from cache: %w", dgst, err)
	}

	req.ContentLength = size

	for _, opt := range options {
		if err := opt(ctx, req, nil, nil); err != nil {
			return fmt.Errorf("unable to apply option to request: %w", err)
		}
	}

	res, err := cl.Do(req)
	if err != nil {
		return fmt.Errorf("unable to upload %s from cache: %w", dgst, err)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusNoContent && res.StatusCode != http.StatusCreated {
		bs, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("unable to upload %s due to %s, and got error reading response: %w", dgst, res.Status, err)
		}

		return fmt.Errorf("unable to upload %s due to %s: %w (message: %s)",
			dgst, res.Status, ErrUnexpectedStatusCode, string(bs))
	}

	return nil
}
