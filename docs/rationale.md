## The Why

The canonically recommended approach to pack software into
images is to use a `Dockerfile`. This file describes all the
steps neccessary to re-build the image later on, even on
different hardware. Consider:

```Dockerfile
FROM debian:latest
COPY /entrypoint.sh /
RUN chmod 0755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
```

Building and pushing the image on my machine at the time of writing
produced the following output:

```
$ docker build -t localhost:5000/hello/world:latest .
Sending build context to Docker daemon  3.072kB
Step 1/4 : FROM debian:latest
latest: Pulling from library/debian
d6ff36c9ec48: Pull complete
Digest: sha256:1e74c92df240634a39d050a5e23fb18f45df30846bb222f543414da180b47a5d
Status: Downloaded newer image for debian:latest
 ---> ee11c54e6bb7
Step 2/4 : COPY /entrypoint.sh /
 ---> a6b7e5e8f5c7
Step 3/4 : RUN chmod 0755 /entrypoint.sh
 ---> Running in e2388bc16415
Removing intermediate container e2388bc16415
 ---> 5aa5445c741a
Step 4/4 : ENTRYPOINT ["/entrypoint.sh"]
 ---> Running in f88411b5d9d0
Removing intermediate container f88411b5d9d0
 ---> c38ecb94b80d
Successfully built c38ecb94b80d
Successfully tagged localhost:5000/hello/world:latest
$ docker push localhost:5000/hello/world:latest
The push refers to repository [localhost:5000/hello/world]
f2c06501b481: Pushed
8c43e76ca884: Pushed
0ced13fcf944: Pushed
latest: digest: sha256:b93f637da3ab7ca12b77453efce0b1729f1eae6cde815bdb45298ce3f1e98046 size: 943
```

Using the value from the last line the image is uniquely identifiable. No one can realistically
send another image and have the same hash value. However, no one else will get the same hash
value by repeating these instructions, and I won't, either.

This is inadequate in a lot of situations:

* It prohibits reproducibility of images,
  each user produces different images given the same inputs.

* Each build, even if the inputs are considered equivalent, is its
  own snowflake and not easily exchangeable to any other build. In consequence,
  it is unknown whether a rebuild (for example, by CI) should happen or have happened.
  It is thus impossible to decide whether a running service should be updated or not without
  having deep knowledge about the image.

* The build is not a pure function of its inputs, but relies on certain environmental
  factors.
